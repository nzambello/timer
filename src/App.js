import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Timer from './Timer.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React timer</h2>
        </div>
        <div className="App-body">
          <Timer />
        </div>
      </div>
    );
  }
}

export default App;
