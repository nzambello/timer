import React, { Component } from 'react';
import './ButtonControl.css';

class ButtonControl extends Component {
  constructor() {
    super();
    this.action = this.action.bind(this);
  }

  action() {
    this.props.action();
  }

  render() {
    return (
      <div className="timer-control">
        <button className="btn btn-default" onClick={() => this.action()}>
          <span>{this.props.text}</span>
        </button>
      </div>
    );
  }
}

export default ButtonControl;
