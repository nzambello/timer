import React, { Component } from 'react';
import './Timer.css';
import DisplayNumber from './DisplayNumber.js';
import ButtonControl from './ButtonControl.js';

class Timer extends Component {
  constructor() {
    super();
    this.state = {
      timer: {
        seconds: 0,
        minutes: 0,
        hours: 0
      },
      going: false
    };

    this.tick = this.tick.bind(this);
    this.toggle = this.toggle.bind(this);
    this.reset = this.reset.bind(this);
  }

  tick() {
    this.setState((prevState, props) => {
      let newState = prevState;
      newState.timer.seconds++;

      if (newState.timer.seconds >= 60) {
        newState.timer.minutes++;
        newState.timer.seconds = 0;

        if (newState.timer.minutes >= 60) {
          newState.timer.hours++;
          newState.timer.minutes = 0;
        }
      }

      return newState;
    });
  }

  play() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  pause() {
    clearInterval(this.timerID);
  }

  toggle() {
    if (this.state.going) {
      this.pause();
    }
    else {
      this.play();
    }

    this.setState((prevState, props) => ({
      going: !prevState.going
    }));
  }

  reset() {
    if (this.state.going) {
      clearInterval(this.timerID);
    }

    this.setState((prevState, props) => {
      let newState = prevState;
      newState.timer.seconds = 0;
      newState.timer.minutes = 0;
      newState.timer.hours = 0;
      newState.going = false;

      return newState;
    });
  }

  componentWillUnmount() {
    if (this.state.going) {
      clearInterval(this.timerID);
    }
  }

  render() {
    let text = this.state.going ? 'Pause' : 'Play';

    return (
      <div className="timer-wrapper">
        <div className="timer-display">
          <DisplayNumber num={this.state.timer.hours} />
          <span>:</span>
          <DisplayNumber num={this.state.timer.minutes} />
          <span>:</span>
          <DisplayNumber num={this.state.timer.seconds} />
        </div>
        <div className="timer-controls">
          <ButtonControl text={text} action={this.toggle} />
          <ButtonControl text="Reset" action={this.reset} />
        </div>
      </div>
    );
  }
}

export default Timer;
