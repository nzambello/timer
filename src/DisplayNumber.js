import React from 'react';
import './DisplayNumber.css';

const DisplayNumber = (props) => {
  let number = props.num.toString();
  if (number.length < 2) {
    number = '0'.concat(number);
  }

  return (
    <div className="display-number">
      <span className="number">{number}</span>
    </div>
  );
};

export default DisplayNumber;
